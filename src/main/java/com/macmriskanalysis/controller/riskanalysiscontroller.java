package com.macmriskanalysis.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import auto_graphdefinition.PhraseInfo;
import auto_graphdefinition.relations;
import controlManager.ControlFromThreat;
import controlManager.SecurityControl;
import threatmanager.manager;
import threatmanager.threat;



@RestController
@RequestMapping("/sla-generator/api/riskAnalysis")
public class riskanalysiscontroller {
	private List<PhraseInfo> phrases=new ArrayList<PhraseInfo>();

	
	@RequestMapping(path="/fileUpload", method=RequestMethod.POST)
	ResponseEntity<?> fileUpload(@RequestParam("file") String Cipher) throws IOException {
		
		System.out.println("OK Request");
		System.out.println(Cipher);	
		relations testrelations=new relations();
		testrelations.delete();//Delete all Graph
		testrelations.runcipher(Cipher); //Run User's Cipher 
		//Recupero info mancanti su relazioni uses e connects
		this.phrases=testrelations.get_phrases();	
		//Costruzione json string
		String json=new Gson().toJson(phrases);
		//Response
		return new ResponseEntity<String>(json, HttpStatus.OK);
	}
	
	
	@RequestMapping(path="/PhraseUpload", method=RequestMethod.POST)
	ResponseEntity<List<threat>> phraseUpload(@RequestBody String object) throws IOException, SQLException {
		List<threat> threat=new ArrayList<threat>();
		System.out.println("OK Request uploadPhraseToUrl");
		JSONObject checkList=new JSONObject(object);
		relations upload=new relations();
		upload.set_Role(phrases, checkList);
		manager Manager=new manager();
		threat=Manager.run();
		
		//Remove duplicate using a Set instead of ArrayList
//		Set<threat> hs = new HashSet<>();
//		hs.addAll(threat);
//		threat.clear();
//		threat.addAll(hs);
		
		List<threat> l1 = new ArrayList<threat>();
//		ArrayList l2 = new ArrayList();

		Iterator iterator = threat.iterator();
		boolean insert=true;
		        while (iterator.hasNext())
		        {
		            threat o = (threat) iterator.next();
		           for(int i=0;i<l1.size();i++) {
		        	   if( l1.get(i).name.equals(o.name) ) {
		        		   insert=false;
		        	   }
		           }
		           if(insert==true) {l1.add(o);}
		        }
		

		return new ResponseEntity<List<threat>>(l1, HttpStatus.OK);
	}
	
	
	
	@RequestMapping(path="/saveAnalysis", method=RequestMethod.POST)
	ResponseEntity<List<SecurityControl>> saveAnalysis(@RequestBody String object) throws IOException, SQLException, ClassNotFoundException, JSONException {
		List<SecurityControl> controls=new ArrayList<SecurityControl>();
		JSONArray threat=new JSONArray(object);
		ControlFromThreat find=new ControlFromThreat();
		for(int i=0; i<threat.length();i++) {
			JSONObject jsn=threat.getJSONObject(i);
			String threatname=(String) jsn.get("name");
			String threatrisk=(String) jsn.get("riskSeverity");
			Boolean insert=true;
			String temp1,temp2;
			
			List<SecurityControl> listaSec=find.searchControl(  threatname,threatrisk );
//			for(int n=0;n<controls.size();n++) {
//				temp1=controls.get(n).properties.get("name");
//				for(int j=0;j<listaSec.size();j++) {
//					temp2=listaSec.get(j).properties.get("name");
//					
//					if(temp1.equals(temp2)) {
//						listaSec.remove(j);
//					}	
//				}
//			}
			controls.addAll( listaSec )  ;
		}

		
		return new ResponseEntity (controls,HttpStatus.OK);
	}
	
	
}



