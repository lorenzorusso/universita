package com.macmriskanalysis.controller;

import java.io.File;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;



@Controller
public class appcontroller {

	@RequestMapping("/")
	String home(ModelMap modal) {
		modal.addAttribute("title","Sla Generator V2");
		return "index";
	}

//	@RequestMapping(path = "upload_file", method = RequestMethod.POST)
//	String upload(@RequestParam(value = "myFile") File cipher) {
				
//		return "index";
//	}

//
	@RequestMapping("/{page}")
	String pageHandler(@PathVariable("page") final String page, @RequestHeader HttpHeaders headers) {

		for (String key : headers.keySet()) {
			System.out.println("Key:"+key+" Value:"+headers.get(key));
		}
		
		System.out.println("REFERER of request: "+headers.get(headers.REFERER));
		return page;
		}
}
