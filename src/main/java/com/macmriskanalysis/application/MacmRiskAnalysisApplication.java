package com.macmriskanalysis.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages={"com.macmriskanalysis.application"})// same as @Configuration @EnableAutoConfiguration @ComponentScan
@ComponentScan(basePackages="com.macmriskanalysis.controller, com.macmriskanalysis.service")
@EntityScan(basePackages="com.macmriskanalysis.entity")
@EnableJpaRepositories(basePackages="com.macmriskanalysis.repositories")
public class MacmRiskAnalysisApplication {

	public static void main(String[] args) {
		SpringApplication.run(MacmRiskAnalysisApplication.class, args);
	}
}
