var app = angular.module('slaGenerator',['ui.router','ngStorage','angular-loading-bar']);


app.constant('urls', {
	BASE: 'http://localhost:8080/sla-generator',
	APPSETUP: 'sla-generator/api/appSetup',
	RISKANALYSIS : 'sla-generator/api/riskAnalysis',
	SLAGENERATOR_API : 'sla-generator/api/slaGeneration',
	ASSESSMENT_NEW_API : 'sla-generator/api/assessmentnew',
	MANAGESLA_API : 'sla-generator/api/manageSla',
	COMPOSER : 'sla-generator/api/composer'
});

app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider',
	function($stateProvider, $urlRouterProvider, $locationProvider) {
	
	$urlRouterProvider.otherwise('/');

	var header = {
			templateUrl: 'header',
			controller:'HeaderController',
			controllerAs:'ctrl'
	}
	var footer = {
			templateUrl: 'footer'
	}

	$stateProvider
	.state('home', {
		url: '/',
		views: {
			"content": {
				templateUrl: 'risk_analysis',
				controller:null,
				controllerAs:'ctrl'
			},
			"header": header,
			"footer": footer
		}
	})
	.state('app_setup', {
		url: '/appsetup',
		views: {
			"content": {
				templateUrl: 'app_setup',
				controller:'AppSetupController',
				controllerAs:'ctrl'
			},
			"header": header,
			"footer": footer
		}
	})
	.state('app_compose', {
		url: '/compose',
		views: {
			"content": {
				templateUrl: 'compose',
				controller: 'ComposeController',
				controllerAs:'ctrl'
			},
			"header": header,
			"footer": footer
		}
	})
	.state('appviewer', {
		url: '/appviewer',
		views: {
			"content": {
				templateUrl: 'app_viewer_v2',
				controller:'AppViewControllerV2',
				controllerAs:'ctrl'
			},
			"header": header,
			"footer": footer
		}
	})
	.state('riskanalysis', {
		url: '/riskanalysis',
		views: {
			"content": {
				templateUrl: 'risk_analysis',
				controller:'RiskAnalysisController',
				controllerAs:'ctrl'
			},
			"header": header,
			"footer": footer
		}
	})
	.state('slagenerator', {
		url: '/slagenerator',
		views: {
			"content": {
				templateUrl: 'slagenerator',
				controller:'SlaGenerationController',
				controllerAs:'ctrl'
			},
			"header": header,
			"footer": footer
		}
	})
	.state('slaviewer', {
		url: '/slaviewer',
		views: {
			"content": {
				templateUrl: 'slaviewer_page',
				controller:'SlaViewerController',
				controllerAs:'ctrl'
			},
			"header": header,
			"footer": footer
		}
	})
	.state('implementationPlan', {
		url: '/implementationPlan',
		views: {
			"content": {
				templateUrl: 'implementationPlan',
				controller:'ImplementationPlanController',
				controllerAs:'ctrl'
			},
			"header": header,
			"footer": footer
		}
	})
	.state('assessment', {
		url: '/assessment',
		views: {
			"content": {
				templateUrl: 'assessment_page',
				controller:'AssessmentPageController',
				controllerAs:'ctrl'
			},
			"header": header,
			"footer": footer
		}
	});
}]);

