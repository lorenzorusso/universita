'use strict';

angular.module('slaGenerator').controller('AppViewControllerV2',
		['$sessionStorage', 'AppViewServiceV2', '$scope', '$location', '$state',  function( $sessionStorage, AppViewServiceV2, $scope, $location, $state) {

			console.log("AppViewController called");

			var self = this;

			//Global Variables
			console.log("AppSetupController -> musa jwt token: "+$sessionStorage.musaJwtToken);
			console.log("AppSetupController -> musa user id: "+$sessionStorage.musaUserId);

			//Variables
			self.application = $sessionStorage.application;
			self.compTypes = ["CUSTOM WEB APP","IDM","STORAGE AS SERVICE", "AGENT","IoT Device","IoT User","Service","Network","GateBee","IoT Mobile Device"];
			self.selectedCompType = "CUSTOM WEB APP";
			self.compname = "";

			//Functions
			self.changeSelectedComponent = changeSelectedComponent;
			self.updateAppState = updateAppState;
			self.addNewComponent = addNewComponent;
			self.submitGeneratedSLA = submitGeneratedSLA;
			self.submitAssessedSLA = submitAssessedSLA;
			
			console.log(self.application);
			updateAppState();

			function changeSelectedComponent(selectedComponent){

				for( var i = 0; i < $sessionStorage.application.components.length; i++){
					if($sessionStorage.application.components[i].id == $sessionStorage.application.activeComponent.id
							&& $sessionStorage.application.components[i].kanbanId == $sessionStorage.application.activeComponent.kanbanId){
						$sessionStorage.application.components[i] = $sessionStorage.application.activeComponent;
						break;
					}
				}

				$sessionStorage.application.activeComponent = selectedComponent;
				self.application = $sessionStorage.application;
				$state.reload();
			}

			function updateAppState(){
				return AppViewServiceV2.updateAppState(self.application.id)
				.then(function (response) {
					console.log('App state get successfully '+JSON.stringify(response));
					$sessionStorage.application.state = response.data;
					self.application = $sessionStorage.application;
				},
				function (errResponse) {
					console.error('Error while get app state');
					self.errorMessage = 'Error while get Metrics: ' + errResponse.data.errorMessage;
				});
			}

			function addNewComponent(){
				return AppViewServiceV2.addNewComponent(self.application.id, self.compname, self.selectedCompType)
				.then(function (response) {
					$sessionStorage.application = {id : null, kanbanId : null, name : null, state : null, allSlatSync : false, slaId : null, 
							activeComponent : 
							{id : null, kanbanId : null, name : null, state : null, riskJson : null, metrics : null, slaId : null, slaXml : null, 
								assessedMetrics : null, assessedSlaId : null, assessedSlaXml : null, composedSlaId : null, composedSlaXml : null, 
								questions : null, slatSync: null, componentGroup: null, threatQuestions : null, threats : null, controls : null},
								components : []};
					self.application = $sessionStorage.application;

					console.log('Component added successfully '+JSON.stringify(response));
					console.log('App get successfully '+JSON.stringify(response.data));
					var appDownloaded = response.data;
					$sessionStorage.application.id = appDownloaded.id;
					$sessionStorage.application.kanbanId = appDownloaded.kanbanId;
					$sessionStorage.application.name = appDownloaded.name;
					$sessionStorage.application.state = appDownloaded.state;

					var componentsDwnld = appDownloaded.components;
					console.log('componentsDwnld length: '+componentsDwnld.length);

					for (var i = 0; i < componentsDwnld.length; i++) { 
						var component = {id : componentsDwnld[i].id, kanbanId : componentsDwnld[i].kanbanId, 
								name : componentsDwnld[i].name, state : componentsDwnld[i].state, riskJson : null, metrics : null, slaId : componentsDwnld[i].slaId, slaXml : null, 
								assessedMetrics : null, assessedSlaId : componentsDwnld[i].assessedSlaId, assessedSlaXml : null, questions : null, slatSync : componentsDwnld[i].slatSync, componentGroup : componentsDwnld[i].componentGroup}
						$sessionStorage.application.components.push(component);
					}

					self.application = $sessionStorage.application;
					alert('Component added successfully!');
					$state.reload();
				},
				function (errResponse) {
					console.error('Error while add component '+JSON.stringify(errResponse));
					self.errorMessage = 'Error while add component: ' + errResponse.data.message;
					alert('Error while add component: '+errResponse.data.message);
				});

			}

			function submitGeneratedSLA(component){
				console.log("Component ID: "+component.id);
				console.log("Component SlaID: "+component.slaId);
				return AppViewServiceV2.setRequiredSLA(component.slaId, component.id, $sessionStorage.musaJwtToken, $sessionStorage.musaUserId)
				.then(function (response) {
					
					console.log('Component sla set successfully ');
					var componentDownloaded = response.data;
					
					for (var i = 0; i < $sessionStorage.application.components.length; i++) { 
						if($sessionStorage.application.components[i].id == component.id){
							$sessionStorage.application.components[i] = componentDownloaded;
						}
					}

					self.application = $sessionStorage.application;
					alert('SLA added successfully!');
					$state.reload();
				},
				function (errResponse) {
					console.error('Error while set SLA on component '+JSON.stringify(errResponse));
					self.errorMessage = 'Error while set SLA on component: ' + errResponse.data.message;
					alert('Error while set SLA on component: '+errResponse.data.message);
				});

			}
			
			function submitAssessedSLA(component){
				console.log("Component ID: "+component.id);
				console.log("Component AssessedSlaID: "+component.assessedSlaId);

				return AppViewServiceV2.setAssessedSLA(component.assessedSlaId, component.id, $sessionStorage.musaJwtToken, $sessionStorage.musaUserId)
				.then(function (response) {
					
					console.log('Component sla set successfully ');
					var componentsDwnld = response.data;
					
					for (var i = 0; i < $sessionStorage.application.components.length; i++) { 

						for (var j = 0; j < componentsDwnld.length; j++) { 
							if(componentsDwnld[j].id == $sessionStorage.application.components[i].id){
								var component = {id : componentsDwnld[j].id, kanbanId : componentsDwnld[j].kanbanId, 
										name : componentsDwnld[j].name, state : componentsDwnld[j].state, riskJson : null, metrics : null, slaId : componentsDwnld[j].slaId, slaXml : null, 
										assessedMetrics : null, assessedSlaId : componentsDwnld[j].assessedSlaId, assessedSlaXml : null, questions : null, slatSync : componentsDwnld[j].slatSync, componentGroup: componentsDwnld[j].componentGroup}
								$sessionStorage.application.components[i] = component;								
							}
							if(componentsDwnld[j].id == $sessionStorage.application.activeComponent.id){
								$sessionStorage.application.activeComponent = {id : null, kanbanId : null, name : null, state : null, riskJson : null, metrics : null, slaId : null, slaXml : null, 
										assessedMetrics : null, assessedSlaId : null, assessedSlaXml : null, composedSlaId : null, composedSlaXml : null, 
										hasQuestions : null, slatSync: null, componentGroup: null, threatQuestions : null, threats : null, controls : null};
								$sessionStorage.application.activeComponent.id = componentsDwnld[j].id;
								$sessionStorage.application.activeComponent.kanbanId = componentsDwnld[j].kanbanId;
								$sessionStorage.application.activeComponent.state = componentsDwnld[j].state;
								$sessionStorage.application.activeComponent.name = componentsDwnld[j].name;
								$sessionStorage.application.activeComponent.slaId = componentsDwnld[j].slaId;
								$sessionStorage.application.activeComponent.assessedSlaId = componentsDwnld[j].assessedSlaId;
								$sessionStorage.application.activeComponent.slatSync = componentsDwnld[j].slatSync;
								$sessionStorage.application.activeComponent.componentGroup = componentsDwnld[j].componentGroup;
								$sessionStorage.application.activeComponent.composedSlaId = componentsDwnld[j].comosedSlaId;								
							}
						}
					}

					self.application = $sessionStorage.application;
					alert(response.message);
					$state.reload();
				},
				function (errResponse) {
					console.error('Error while set SLA on component '+JSON.stringify(errResponse));
					self.errorMessage = 'Error while set SLA on component: ' + errResponse.data.message;
					alert('Error while set SLA on component: '+errResponse.data.message);
				});
			}


		}]);