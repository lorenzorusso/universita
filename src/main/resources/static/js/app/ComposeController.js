'use strict';

angular.module('slaGenerator').controller('ComposeController',
		['$sessionStorage', 'ComposeService','$scope', '$state', '$location', function( $sessionStorage, ComposeService, $scope, $state, $location) {

			var self = this;

			console.log("ComposeController -> musa jwt token: "+$sessionStorage.musaJwtToken);
			console.log("AppSetupController -> musa user id: "+$sessionStorage.musaUserId);

			//Variables
			self.application = $sessionStorage.application;
			self.allControls = [];
			self.allGeneratedControls = [];
			self.allAssessedControls = [];
			self.allComposedControls = [];

			self.generatedControls = [];
			self.assessedControls = [];
			self.composedControls = [];

			//Functions
			self.composeApplication = composeApplication;
			self.retieveComposedSLAS = retieveComposedSLAS;

			console.log(self.application);
			loadSlas();

			function loadSlas(){
				self.allControls = [];
				self.allGeneratedControls = [];
				self.allAssessedControls = [];
				self.allComposedControls = [];
				self.generatedControls = [];
				self.assessedControls = [];
				self.composedControls = [];
				if($sessionStorage.application.activeComponent.slaId != null){
					console.log("GENERATED SLA ID: "+self.application.activeComponent.slaId)
					ComposeService.loadSlaContent(self.application.activeComponent.slaId)
					//ComposeService.loadSlaContent('59BA863B77C827DB1C4882ED')
					.then(function (response) {
						console.log('Sla Content Get successfully');
						if(response.data != null){
							var generatedSlaXml = response.data.replace(/<(\/?)([^:>\s]*:)?([^>]+)>/g, "<$1$3>");
							var generatedSlaJson = $.xml2json($.parseXML(generatedSlaXml));

							setAllControls(generatedSlaJson, self.allGeneratedControls);
							loadAssessedSla();
						}
					},
					function (errResponse) {
						console.error('Error while getting Sla content');
						loadAssessedSla();
					}
					);
				}else{
					loadAssessedSla();
				}
			}

			function loadAssessedSla(){
				if($sessionStorage.application.activeComponent.assessedSlaId != null){
					console.log("ASSESSED SLA ID: "+self.application.activeComponent.assessedSlaId)
					ComposeService.loadSlaContent(self.application.activeComponent.assessedSlaId)
					//ComposeService.loadSlaContent('59BFE15177C83DC96471F7C2')
					.then(function (response) {
						console.log('Sla Content Get successfully');
						if(response.data != null){
							var assessedSlaXml = response.data.replace(/<(\/?)([^:>\s]*:)?([^>]+)>/g, "<$1$3>");
							var assessedSlaJson = $.xml2json($.parseXML(assessedSlaXml));

							setAllControls(assessedSlaJson, self.allAssessedControls);
							loadComposedSla();
						}
					},
					function (errResponse) {
						console.error('Error while getting Sla content');ù
						loadComposedSla();
					}
					);
				}else{
					loadComposedSla();
				}
			}

			function loadComposedSla(){
				if($sessionStorage.application.activeComponent.composedSlaId != null){
					console.log("COMPOSED SLA ID: "+self.application.activeComponent.composedSlaId)
					ComposeService.loadSlaContent(self.application.activeComponent.composedSlaId)
					//ComposeService.loadSlaContent('59BFE15177C83DC96471F7C2')
					.then(function (response) {
						console.log('Sla Content Get successfully');
						if(response.data != null){
							var composedSlaXml = response.data.replace(/<(\/?)([^:>\s]*:)?([^>]+)>/g, "<$1$3>");
							var composedSlaJson = $.xml2json($.parseXML(composedSlaXml));

							setAllControls(composedSlaJson, self.allComposedControls);
							printLoadedControls();
						}
					},
					function (errResponse) {
						console.error('Error while getting Sla content');
						printLoadedControls();
					}
					);
				}else{
					printLoadedControls();
				}
			}

			function printLoadedControls(){
				console.log("Required Controls: "+self.allGeneratedControls);
				console.log("Assessed Controls: "+self.allAssessedControls);
				console.log("Composed Controls: "+self.allComposedControls);

				var maxLength = self.allGeneratedControls.length;
				if(self.allAssessedControls.length > maxLength){
					maxLength = self.allAssessedControls.length
				}else if(self.allComposedControls.length > maxLength){
					maxLength = self.allComposedControls.length;
				}

				for (var i = 0; i < self.allGeneratedControls.length; i++){
					self.allControls.push(self.allGeneratedControls[i]);
				}

				for (var i = 0; i < self.allAssessedControls.length; i++){
					var controlAlreadyPresent = false;
					for (var j = 0; j < self.allControls.length; j++){
						if(self.allAssessedControls[i].id == self.allControls[j].id){
							controlAlreadyPresent = true;
							break;
						}
					}
					if(!controlAlreadyPresent){
						self.allControls.push(self.allAssessedControls[i]);
					}
				}

				for (var i = 0; i < self.allComposedControls.length; i++){
					var controlAlreadyPresent = false;
					for (var j = 0; j < self.allControls.length; j++){
						if(self.allComposedControls[i].id == self.allControls[j].id){
							controlAlreadyPresent = true;
							break;
						}
					}
					if(!controlAlreadyPresent){
						self.allControls.push(self.allComposedControls[i]);
					}
				}

				//Set of required controls
				for (var i = 0; i < self.allControls.length; i++){
					var controlIsInGenerated = false;
					for (var j = 0; j < self.allGeneratedControls.length; j++){
						if(self.allControls[i].id == self.allGeneratedControls[j].id){
							controlIsInGenerated = true;
							break;
						}
					}
					if(controlIsInGenerated){
						self.generatedControls.push(true);
					}else{
						self.generatedControls.push("Not Available");
					}
				}

				//Set of assessed controls
				if(self.allAssessedControls.length > 0){
					for (var i = 0; i < self.allControls.length; i++){
						var controlIsInGenerated = false;
						for (var j = 0; j < self.allGeneratedControls.length; j++){
							if(self.allControls[i].id == self.allGeneratedControls[j].id){
								controlIsInGenerated = true;
								break;
							}
						}

						var controlIsInAssessed = false;
						for (var j = 0; j < self.allAssessedControls.length; j++){
							if(self.allControls[i].id == self.allAssessedControls[j].id){
								controlIsInAssessed = true;
								break;
							}
						}

						if(controlIsInGenerated && controlIsInAssessed){
							self.assessedControls.push(true);
						}else if(controlIsInGenerated && !controlIsInAssessed){
							self.assessedControls.push(false);
						}else if(!controlIsInGenerated && controlIsInAssessed){
							self.assessedControls.push(true);
						}else{
							self.assessedControls.push("Not Available");
						}
					}
				}else{
					for (var i = 0; i < maxLength; i++){
						self.assessedControls.push("Not Available");
					}
				}

				//Set of composed controls
				if(self.allComposedControls.length > 0){
					for (var i = 0; i < self.allControls.length; i++){
						var controlIsInGenerated = false;
						for (var j = 0; j < self.allGeneratedControls.length; j++){
							if(self.allControls[i].id == self.allGeneratedControls[j].id){
								controlIsInGenerated = true;
								break;
							}
						}

						var controlIsInAssessed = false;
						for (var j = 0; j < self.allAssessedControls.length; j++){
							if(self.allControls[i].id == self.allAssessedControls[j].id){
								controlIsInAssessed = true;
								break;
							}
						}

						var controlIsInComposed = false;
						for (var j = 0; j < self.allComposedControls.length; j++){
							if(self.allControls[i].id == self.allComposedControls[j].id){
								controlIsInComposed = true;
								break;
							}
						}

						if(controlIsInGenerated && controlIsInAssessed && controlIsInComposed){
							self.composedControls.push(true);
						}else if(controlIsInGenerated && controlIsInAssessed && !controlIsInComposed){
							self.composedControls.push(false);
						}else if(controlIsInGenerated && !controlIsInAssessed && controlIsInComposed){
							self.composedControls.push(true);
						}else if(controlIsInGenerated && !controlIsInAssessed && !controlIsInComposed){
							self.composedControls.push(false);
						}else if(!controlIsInGenerated && controlIsInAssessed && controlIsInComposed){
							self.composedControls.push(true);
						}else if(!controlIsInGenerated && controlIsInAssessed && !controlIsInComposed){
							self.composedControls.push(false);
						}else if(!controlIsInGenerated && !controlIsInAssessed && controlIsInComposed){
							self.composedControls.push(true);
						}else if(!controlIsInGenerated && !controlIsInAssessed && !controlIsInComposed){
							self.composedControls.push("not available");
						}
					}
				}else{
					for (var i = 0; i < maxLength; i++){
						self.composedControls.push("Not Available");
					}
				}

			}

			function setAllControls(slaJson, controlsArray){
				var capabilities = slaJson.Terms.All.ServiceDescriptionTerm.serviceDescription.capabilities;
				if(isArray(capabilities.capability)){
					console.log("capabilities.capability is array");
					var controls = capabilities.capability[0].controlFramework.NISTsecurityControl;
					if(isArray(controls)){
						for (var i = 0; i < controls.length; i++){
							controlsArray.push(controls[i]);
						}
					}else{
						controlsArray.push(controls);
					}
				}
				else{
					console.log("capabilities.capability is not array");
					var controls = capabilities.capability.controlFramework.NISTsecurityControl;
					if(isArray(controls)){
						for (var i = 0; i < controls.length; i++){
							controlsArray.push(controls[i]);
						}
					}else{
						controlsArray.push(controls);
					}
				}
			}


			function composeApplication(){
				return ComposeService.composeApplication(self.application.id, $sessionStorage.musaJwtToken, $sessionStorage.musaUserId)
				.then(function (response) {
					console.log('Application composed successfully '+JSON.stringify(response));
					alert("Application composed successfully!\n");
					if(self.application.state == "SLATs_GENERATED"){
						self.application.state = "SLAs_GENERATED";
					}else if(self.application.state == "SLATs_ASSESSED"){
						self.application.state = "ASSESSED_SLAs_GENERATED";
					}
					for (var i = 0; i < self.application.components.length; i++){
						self.application.components[i].state = "SLA_GENERATED";
					}
					self.application.activeComponent.state = "SLA_GENERATED";
				},
				function (errResponse) {
					console.error('Error while compose Application' + errResponse.data.message);
					self.errorMessage = 'Error while compose Application: ' + errResponse.data.message;
					alert('Error while compose Application!\n' + errResponse.data.message);
				}
				);
			}


			function retieveComposedSLAS(){
				return ComposeService.retieveComposedSLAS(self.application.id, $sessionStorage.musaJwtToken, $sessionStorage.musaUserId)
				.then(function (response) {
					console.log('Composed SLAS retieved successfully ');
					alert("Composed SLAS retieved successfully!\n");

					for(var i = 0; i < self.application.components.length; i++){
						for(var j = 0; j < response.data.length; j++){
							console.log("Comp id comparison: "+self.application.components[i].id+" - "+response.data[j].id);
							if(self.application.components[i].id == response.data[j].id){
								console.log("Composed sla id found: "+response.data[j].comosedSlaId);
								self.application.components[i].composedSlaId = response.data[j].comosedSlaId;
								if(self.application.activeComponent.id == self.application.components[i].id){
									self.application.activeComponent.composedSlaId = response.data[j].comosedSlaId;
								}
							}
						}

					}

					$state.reload();
				},
				function (errResponse) {
					console.error('Error while retieve Composed SLAS' + errResponse.data.message);
					self.errorMessage = 'Error while retieve Composed SLAS: ' + errResponse.data.message;
					alert('Error while retieve Composed SLAS!\n' + errResponse.data.message);
				}
				);
			}

			function isArray(object) {
				if(Object.prototype.toString
						.call(object) === '[object Array]') {

					return true;
				}
				return false;
			}

		}]);