'use strict';

angular.module('slaGenerator').controller('RiskAnalysisController',
		['$sessionStorage', 'RiskAnalysisService', '$http','$scope', '$location', '$state', '$window',  function( $sessionStorage, RiskAnalysisService,$http, $scope, $location, $state, $window) {

			console.log("AppViewController called");

			var self = this;
			
			console.log("RiskAnalysisController -> musa jwt token: "+$sessionStorage.musaJwtToken);
			console.log("AppSetupController -> musa user id: "+$sessionStorage.musaUserId);

			//Variables
			self.io;
			self.checkList=checkList;
			self.threat;
			self.threatnum;
			self.controls;
			self.textphrases;
			self.application = $sessionStorage.application;
			self.selectedTab = "Start";
			self.phrases='OFF';
//			self.ROLE=[	"client","server","resource_server","auth_server","resource_owner"];
			
			self.ROLE=[
				{
					value:"client",
					id:"1"
				},
				{
					value:"server",
					id:"2"
				},
				{
					value:"resource_server",
					id:"3"
				},
				{
					value:"auth_server",
					id:"4"
				},
				{
					value:"resource_owner",
					id:"5"
				}
				
			];
			
			
			self.stride=["SPOOFING","TAMPERING","REPUDIATION","INFORMATION DISCLOSURE","DENIAL OF SERVICE","ELEVATION OF PRIVILEGES"];
			
			self.PROTOCOL=[
				{
					value:"xmpp",
						id:"1"
				},
				{
					value:"ssl",
						id:"2"
				},
				{
					value:"tls",
						id:"3"
				},
				{
					value:"zigbee",
						id:"4"
				},
				{
					value:"tcp",
						id:"5"
				},
				{
					value:"ip",
						id:"6"
				},
				{
					value:"http",
						id:"7"
				},
				{
					value:"https",
						id:"8"
				},
				{
					value:"oauth",
						id:"9"
				},
				{
					value:"ethernet",
						id:"10"
				}
			];
			
			console.log(self.application);
			
			self.add = add;
			//Functions
			
			self.submitPhrase=submitPhrase;
			//self.uploadFile=uploadFile;
			self.saveAnalysis=saveAnalysis;
			self.startRiskAnalysis = startRiskAnalysis;
			self.startLorenzo = startLorenzo;
			self.loadQuestions = loadQuestions;
			self.submitQuestions = submitQuestions;
			self.updateRiskEvaluation = updateRiskEvaluation;
			self.saveEvaluation = saveEvaluation;
			self.getStrideRisk = getStrideRisk;
			self.submitControls = submitControls;
			
			self.nextToRiskEvaluation = nextToRiskEvaluation;
			self.nextToControlsSelection = nextToControlsSelection;
			self.nextToAnalysisResult = nextToAnalysisResult;

			$scope.getTabSelected = function (tabName) {
				return (tabName === self.selectedTab) ? 'active' : '';
			}

			function startRiskAnalysis(){
				self.selectedTab = "Threats Selection";
			}
			function startLorenzo(){

				self.selectedTab = "MACM Upload";
			}

			function loadQuestions(){
				return RiskAnalysisService.getComponentQuestions(self.application.activeComponent.id)
				.then(function (response) {
					console.log('Questions get successfully '+JSON.stringify(response));
					$sessionStorage.application.activeComponent.threatQuestions = response;
					for (var i = 0 ; i < $sessionStorage.application.activeComponent.threatQuestions.length; i++) {
						console.log("add selected");
						$sessionStorage.application.activeComponent.threatQuestions[i].selected = "true";
					}
					self.application = $sessionStorage.application;
					alert("Questions are successfully loaded!\nPlease answer the questions!");
				},
				function (errResponse) {
					console.error('Error while get Questions ' + errResponse.data.message);
					self.errorMessage = 'Error while get Questions: ' + errResponse.data.message;
					alert('Error while get Questions!\n' + errResponse.data.message);
				}
				);
			}

			function submitQuestions(){
				return RiskAnalysisService.submitQuestions(self.application.activeComponent.id, self.application.activeComponent.threatQuestions)
				.then(function (response) {
					console.log('Threats get successfully '+JSON.stringify(response));
					$sessionStorage.application.activeComponent.threats = response;
					self.application = $sessionStorage.application;
					alert("Threats are successfully loaded!\n");
				},
				function (errResponse) {
					console.error('Error while get Threats ' + errResponse.data.message);
					self.errorMessage = 'Error while get Threats: ' + errResponse.data.message;
					alert('Error while get Threats!\n' + errResponse.data.message);
				}
				);
			}

			function updateRiskEvaluation(){
				for ( var i = 0; i < self.threat.length; i++){
					var totaleImpactBusiness = (self.threat[i].properties.financial_damage +
							self.threat[i].properties.reputation_damage +
							self.threat[i].properties.non_compliance +
							self.threat[i].properties.privacy_violation) / 4;

					var totaleImpactTechnical = (self.threat[i].properties.loss_of_confidentiality +
							self.threat[i].properties.loss_of_integrity +
							self.threat[i].properties.loss_of_availability +
							self.threat[i].properties.loss_of_accountability) / 4;

					if(totaleImpactBusiness==0){
						self.threat[i].risk.impact=totaleImpactTechnical;
					}else{
						self.threat[i].risk.impact=totaleImpactBusiness;
					}

					
					if(self.threat[i].risk.impact >= 6){
						if (self.threat[i].risk.likehood < 3){
							self.threat[i].riskSeverity = "MEDIUM";
						}else if (self.threat[i].risk.likehood >= 3 && 
								self.threat[i].risk.likehood < 6){
							self.threat[i].riskSeverity = "HIGH";
						}else if (self.threat[i].risk.likehood >= 6){
							self.threat[i].riskSeverity = "CRITICAL";
						}
					}else if(self.threat[i].risk.impact >= 3 && 
							self.threat[i].risk.impact < 6){
						if (self.threat[i].risk.likehood < 3){
							self.threat[i].riskSeverity = "LOW";
						}else if (self.threat[i].risk.likehood >= 3 && 
								self.threat[i].risk.likehood < 6){
							self.threat[i].riskSeverity = "MEDIUM";
						}else if (self.threat[i].risk.likehood >= 6){
							self.threat[i].riskSeverity = "HIGH";
						}
					}else if(self.threat[i].risk.impact < 3){
						if (self.threat[i].risk.likehood < 3){
							self.threat[i].riskSeverity = "VERY LOW";
						}else if (self.threat[i].risk.likehood >= 3 && 
								self.threat[i].risk.likehood < 6){
							self.threat[i].riskSeverity = "LOW";
						}else if (self.threat[i].risk.likehood >= 6){
							self.threat[i].riskSeverity = "MEDIUM";
						}
					}
//					console.log("severity: "+self.threat[i].riskSeverity);
				}
//				console.log("impact: "+self.threat[i].risk.impact);
//				console.log("likehood: "+self.threat[i].risk.likehood);
				
				$sessionStorage.application = self.application;
			}
			
			
			function saveAnalysis(){
		
					var saveAnalysis = "/saveAnalysis";
					RiskAnalysisService.uploadSavedAnalysis(self.threat, saveAnalysis)
					 .then(function(response){
		            	//self.controls=response;
		            	var myControls = JSON.stringify(response);
		            	self.controls=JSON.parse(myControls);
		            	console.log(self.controls);
		            });
				}
				

			function saveEvaluation(){
				return RiskAnalysisService.saveEvaluation(self.application.activeComponent.id, self.application.activeComponent.threats,self.getStrideRisk('SPOOFING'), self.getStrideRisk('TAMPERING'), self.getStrideRisk('REPUDIATION'), self.getStrideRisk('INFORMATION DISCLOSURE'), self.getStrideRisk('DENIAL OF SERVICE'), self.getStrideRisk('ELEVATION OF PRIVILEGES'))
				.then(function (response) {
					console.log('Evaluation saved successfully '+JSON.stringify(response));
					$sessionStorage.application.activeComponent.controls = response;
					self.application = $sessionStorage.application;
					alert("Evaluation saved successfully!\n");
				},
				function (errResponse) {
					console.error('Error while save evaluation ' + errResponse.data.message);
					self.errorMessage = 'Error while save evaluation: ' + errResponse.data.message;
					alert('Error while save evaluation!\n' + errResponse.data.message);
				}
				);
			}

			function getStrideRisk(strideName){
				//console.log("getStrideRisk called");
				var strideValue = 0;
				var numOfElements = 0;
				if(self.threat != null && self.threat.length>0){
				for ( var i = 0; i < self.threat.length; i++){
					for ( var j = 0; j < 6; j++){
						if(self.threat[i].strides == strideName){
							if(self.threat[i].riskSeverity == "VERY LOW"){
								strideValue = strideValue + 1;
								numOfElements++;
							}else if(self.threat[i].riskSeverity == "LOW"){
								strideValue = strideValue + 3;
								numOfElements++;
							}else if(self.threat[i].riskSeverity == "MEDIUM"){
								strideValue = strideValue + 5;
								numOfElements++;
							}else if(self.threat[i].riskSeverity == "HIGH"){
								strideValue = strideValue + 7;
								numOfElements++;
							}else if(self.threat[i].riskSeverity == "CRITICAL"){
								strideValue = strideValue + 9;
								numOfElements++;
							}
						}
					}
				}}
				if(self.threatnum > 0){
					strideValue = strideValue / self.threatnum;
					
				}

				if(strideValue>= 0 && strideValue < 2){
					return "VERY LOW";
				}else if(strideValue>= 2 && strideValue < 4){
					return "LOW";
				}else if(strideValue>= 4 && strideValue < 6){
					return "MEDIUM";
				}else if(strideValue>= 6 && strideValue < 8){
					return "HIGH";
				}else if(strideValue>= 8){
					return "CRITICAL";
				}
			}

			function submitControls(){
				return RiskAnalysisService.submitControls(self.application.activeComponent.id, self.application.activeComponent.controls, $sessionStorage.musaJwtToken, $sessionStorage.musaUserId)
				.then(function (response) {
					console.log('Controls saved successfully '+JSON.stringify(response));
					alert("Controls saved successfully!\n");
				},
				function (errResponse) {
					console.error('Error while save Controls ' + errResponse.data.message);
					self.errorMessage = 'Error while save Controls: ' + errResponse.data.message;
					alert('Error while save Controls!\n' + errResponse.data.message);
				}
				);
			}

			function nextToRiskEvaluation(){
				self.updateRiskEvaluation();
				self.selectedTab = 'Risks Evaluation';
				$window.scrollTo(0, 0);
			}

			function nextToControlsSelection(){
				self.selectedTab = 'Controls Selection';
				$window.scrollTo(0, 0);
			}

			function nextToAnalysisResult(){
				self.selectedTab = 'Analysis Results';
				$window.scrollTo(0, 0);
			}
			function nextToAddInfo(){
				self.selectedTab = 'Add Info';
				$window.scrollTo(0, 0);
			}

			function add(){
				console.log("Funziona");
				var file = $scope.myFile;
	            
				console.log('file is ' );
	            console.log(file);
	               
	            var uploadUrl = "/fileUpload";
	            //uploadFile(file, uploadUrl);
	            return "";
			}
			

			var checkList;
			var size;
			$scope.submit=function() {
				$scope.fileContent = '';
			    $scope.fileSize = 0;
			    $scope.fileName = '';
			    $scope.phrares;
			    var file = document.getElementById("myFileInput").files[0];
			    if (file) {
			        var aReader = new FileReader();
			        aReader.readAsText(file, "UTF-8");
			        aReader.onload = function (evt) {
			            $scope.fileContent = aReader.result;
			            $scope.fileName = document.getElementById("myFileInput").files[0].name;
			            $scope.fileSize = document.getElementById("myFileInput").files[0].size;
			            var uploadUrl = "/fileUpload";
			            RiskAnalysisService.uploadFileToUrl($scope.fileContent,uploadUrl)
			            .then(function(response){
			            	self.textphrases=response;
			            	var myJSON = JSON.stringify(self.textphrases);
			            	self.io=JSON.parse(myJSON); 
			            	size=self.io.length;
			            	checkList=[9];
			            	self.phrases='ON';
			            	
			            });
			        }
			    }
		}



		function submitPhrase() {
			var uploadUrlPhrase = "/PhraseUpload";
			RiskAnalysisService.uploadPhraseToUrl(self.checkList, uploadUrlPhrase)
			 .then(function(response){
			            	self.threat=response;
			            	var myJSON = JSON.stringify(response);
			            	self.threat=JSON.parse(myJSON); 
			            	self.threatnum=self.threat.length;
			            	
			            	
			            	
			            });
			self.selectedTab="Result";
		}
		
		
		
		
		

	}]);





