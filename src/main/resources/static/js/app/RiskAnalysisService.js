'use strict';

angular.module('slaGenerator').factory('RiskAnalysisService',
		['$sessionStorage','$localStorage', '$http', '$q', 'urls',
			function ($sessionStorage, $localStorage, $http, $q, urls) {

			var factory = {
					getComponentQuestions : getComponentQuestions,
					submitQuestions : submitQuestions,
					saveEvaluation : saveEvaluation,
					submitControls : submitControls,
					//sendFile : sendFile,
					uploadFileToUrl : uploadFileToUrl,
					uploadPhraseToUrl : uploadPhraseToUrl,
					uploadSavedAnalysis : uploadSavedAnalysis
			};

			return factory;

			
			function getComponentQuestions(componentId){
				var deferred = $q.defer();

				$http(
						{
							method : 'GET',
							url : urls.RISKANALYSIS+"/componentQuestions?componentId="+componentId
						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}
			
			function submitQuestions(compId, questions){
				var deferred = $q.defer();

				$http(
						{
							method : 'POST',
							url : urls.RISKANALYSIS+"/submitQuestions",
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
									obj) {
								var str = [];
								for ( var p in obj)
									str
									.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'compId' : compId,
								'threatQuestions' : JSON.stringify(questions),
							}

						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}
			
			function saveEvaluation(compId, componentThreats, spoofingRisk, tamperingRisk, repudiationRisk, informationDisclosureRisk, denialOfServiceRisk, elevationOfPrivilegesRisk){
				var deferred = $q.defer();

				$http(
						{
							method : 'POST',
							url : urls.RISKANALYSIS+"/saveEvaluation",
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
									obj) {
								var str = [];
								for ( var p in obj)
									str
									.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'compId' : compId,
								'componentThreats' : JSON.stringify(componentThreats),
								'spoofingRisk' : spoofingRisk,
								'tamperingRisk' : tamperingRisk,
								'repudiationRisk' : repudiationRisk,
								'informationDisclosureRisk' : informationDisclosureRisk,
								'denialOfServiceRisk' : denialOfServiceRisk,
								'elevationOfPrivilegesRisk' : elevationOfPrivilegesRisk
							}

						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}
			
			function submitControls(compId, controls, musaJwtToken, musaUserId){
				var deferred = $q.defer();

				$http(
						{
							method : 'POST',
							url : urls.RISKANALYSIS+"/submitControls",
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
									obj) {
								var str = [];
								for ( var p in obj)
									str
									.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'compId' : compId,
								'controls' : JSON.stringify(controls),
								'jwtToken' : musaJwtToken,
								'userId' : musaUserId,
							}

						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}

//			function sendFile(fd){
//				var deferred= $q.defer();
//				console.log("Sono arrivato !!!"+fd);
//				 $http.post('/uploadFile', fd, {
//				        withCredentials: true,
//				        headers: {'Content-Type': undefined },
//				        transformRequest: angular.identity
//				    }).success("" ).error( "");
//				
//				return deferred.promise;
//			}
			
			function uploadFileToUrl (file, uploadUrl)   {

	            console.log(file);
	            var send=new FormData();
	            send.append('file', file);
	            var deffered = $q.defer();
	            var address=urls.RISKANALYSIS + uploadUrl;
	            
	            $http.post(address, send, {
	                withCredentials : false,
	                transformRequest : angular.identity,
	                headers : { 'Content-Type' : undefined }
	            }).then(function(response){
	                // ANything you wanted to do with response
	            	deffered.resolve(response.data);
	            	console.log("OK Request-Response");
	            	
	            	
	            })

	            return deffered.promise;
	        }
			
			
			function uploadPhraseToUrl (check,Url)   {
				console.log("Chiamato Service uploadPhraseToUrl");
				
				var address=urls.RISKANALYSIS + Url;
//				var send=new FormData();
//				send.append('checklist', check);
				console.log(check);
				var json=JSON.stringify(check)
				
				var deffered = $q.defer();
//				console.log(send);
				$http.post(address, json, {
	                withCredentials : false,
	                transformRequest : angular.identity,
	                headers : { 'Content-Type' : undefined}
	            }).then(function(response){
	                // ANything you wanted to do with response
	            	deffered.resolve(response.data);
	            	console.log("OK uploadPhraseToUrl");
	            	console.log(response.data);
	            })
				return deffered.promise;
				
	        }
			
			
			function uploadSavedAnalysis (threat,Url)   {
				var address=urls.RISKANALYSIS + Url;
				//var oggetto=JSON.parse(threat);
				var json=JSON.stringify(threat);
				console.log(json);
				var deffered = $q.defer();
				$http.post(address, json, {
	                withCredentials : false,
	                transformRequest : angular.identity,
	                headers : { 'Content-Type' : undefined}
	            }).then(function(response){
	                // ANything you wanted to do with response
	            	deffered.resolve(response.data);
	            	console.log("OK uploadSavedAnalysis");
	            	console.log(response.data);
	            })
				return deffered.promise;
        }
			
			
			
			
			
	

		}]);//end




