<div class=container>
	<div class="panel panel-default">
		<div class="panel-heading text-center">
			<span class="lead">
				<h2>Application Details</h2>
			</span>
		</div>

		<div class="panel-body">

			<table class="table table-hover">
				<thead>
					<tr>
						<td style="width: 5%;"><b>Id</b></td>
						<td style="width: 5%;"></td>
						<td style="width: 35%;"><b>Kanban Id</b></td>
						<td style="width: 5%;"></td>
						<td style="width: 20%;"><b>Name</b></td>
						<td style="width: 5%;"></td>
						<td style="width: 25%;"><b>State</b></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="width: 5%;"><p>{{ctrl.application.id}}</p></td>
						<td style="width: 5%;"></td>
						<td style="width: 35%;"><p>{{ctrl.application.kanbanId}}</p></td>
						<td style="width: 5%;"></td>
						<td style="width: 20%;"><p>{{ctrl.application.name}}</p></td>
						<td style="width: 5%;"></td>
						<td style="width: 25%;"><p>{{ctrl.application.state}}</p></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading text-center">
			<span class="lead">
				<h2>Selected Component Details</h2>
			</span>
		</div>

		<div class="panel-body">

			<table class="table table-hover">
				<thead>
					<tr>
						<td style="width: 5%;"><b>Id</b></td>
						<td style="width: 5%;"></td>
						<td style="width: 35%;"><b>Kanban Id</b></td>
						<td style="width: 5%;"></td>
						<td style="width: 20%;"><b>Name</b></td>
						<td style="width: 5%;"></td>
						<td style="width: 25%;"><b>State</b></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="width: 5%;"><p>{{ctrl.application.activeComponent.id}}</p></td>
						<td style="width: 5%;"></td>
						<td style="width: 35%;"><p>{{ctrl.application.activeComponent.kanbanId}}</p></td>
						<td style="width: 5%;"></td>
						<td style="width: 20%;"><p>{{ctrl.application.activeComponent.name}}</p></td>
						<td style="width: 5%;"></td>
						<td style="width: 25%;"><p>{{ctrl.application.activeComponent.state}}</p></td>
					</tr>
				</tbody>
			</table>
			
			<table class="table table-hover">
				<thead>
					<tr>
						<td style="width: 45%;"><b>SLA Id</b></td>
						<td style="width: 5%;"></td>
						<td style="width: 50%;"><b>Assessed SLA Id</b></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="width: 45%;"><p>{{ctrl.application.activeComponent.slaId}}</p></td>
						<td style="width: 5%;"></td>
						<td style="width: 50%;"><p>{{ctrl.application.activeComponent.assessedSlaId}}</p></td>
					</tr>
				</tbody>
			</table>
			
			<div >
				<div class="form-group">
					<label for="sel1">Select Active Component:</label> <select
						class="form-control"
						ng-options="component as component.name for component in ctrl.application.components track by component.name"
						ng-model="ctrl.selectedComponent" ng-change="ctrl.changeSelectedComponent()"></select>
						
				</div>
			</div>
		</div>
	</div>

</div>