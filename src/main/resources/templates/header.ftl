<div id="_navigator">
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">

				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" ui-sref="home"> <img alt="Brand"
					src="images/logo_musa_60.png"></a>

			</div>

			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav navbar-left">
					<li class="dropdown"><a class="dropdown-toggle"
						data-toggle="dropdown">App id: {{ctrl.appId}}<span
							class="caret"></span></a>
						<ul class="dropdown-menu">
							<div align="center">
								<button style="width: 90%" class="btn btn-primary"
									ui-sref="app_setup">SetUp Application</button>
							</div>
							<br>
							<div align="center">
								<button style="width: 90%" class="btn btn-primary"
									ng-click="ctrl.reset()">Reset</button>
							</div>
						</ul></li>
					<li><a>Selected Component:
							{{ctrl.application.activeComponent.name}}</a></li>


				</ul>

				<ul class="nav navbar-nav navbar-right">
					<li ng-class="getClass('/appviewer')"><a ui-sref="appviewer">App
							View</a></li>
					<li ng-class="getClass('/riskanalysis')"><a
						ui-sref="riskanalysis">Risk Analysis</a></li>
					<li ng-class="getClass('/slagenerator')"><a
						ui-sref="slagenerator">SLA Generation</a></li>
					<li ng-class="getClass('/assessment')"><a ui-sref="assessment">SLA
							Assessment</a></li>
					<li ng-class="getClass('/slaviewer')"><a ui-sref="slaviewer">SLA
							View</a></li>
					<li ng-class="getClass('/compose')"><a ui-sref="app_compose">SLA
							Compose</a></li>
					<li ng-class="getClass('/implementationPlan')"><a
						ui-sref="implementationPlan">Implementation Plan</a></li>

					<li ><a></a></li>

				</ul>



			</div>

		</div>
	</nav>
</div>