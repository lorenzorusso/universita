<div class=container>
	<div class="panel panel-default">
		<div class="panel-heading text-center">
			<span class="lead">
				<h2>SLA Composition</h2>
			</span>
		</div>

		<div class="panel-body">

			<div
				ng-show="ctrl.application.id == null || ctrl.application.id == ''">
				There isn't an application to execute Sla Composition.<br> To
				start with the SLA Cmposition select "SetUp Application" button on
				top right of header bar and load an application.
			</div>

			<div
				ng-show="ctrl.application.id != null && ctrl.application.id != ''">

				<p>To execute the SLA composition click the button below.</p>
				<p>After the composition, to show the Composed SLAS, click the
					button "Retrieve Composed SLAS"</p>
				<br>
				<div style="text-align: center;">
					<button style="text-align: center;" class="btn btn-primary"
						ng-click="ctrl.composeApplication()">Compose Application</button>

					<br> <br>
					<button style="text-align: center;" class="btn btn-primary"
						ng-click="ctrl.retieveComposedSLAS()"
						ng-disabled="ctrl.application.state!='ASSESSED_SLAs_GENERATED' && ctrl.application.state!='SLAs_GENERATED'">Retrieve
						Composed SLAS</button>
				</div>
			</div>


			<br>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a>Feedback on {{ctrl.application.activeComponent.name}}</a>
					</h4>
				</div>
				<div>
					<div class="panel-body ">


						<table class="table table-striped " style="width: 100%"
							ng-show="ctrl.allControls.length > 0">
							<thead>
								<tr>
									<td style="width: 25%;"><b>Security Controls</b></td>
									<td style="width: 25%; text-align: center;"><b>Requirement</b></td>
									<td style="width: 25%; text-align: center;"><b>Assessed</b></td>
									<td style="width: 25%; text-align: center;"><b>Granted</b></td>
								</tr>
							</thead>
							<tbody>
								<tr
									ng-repeat="item in ctrl.allControls | orderBy:'+control_family'">

									<td style="width: 25%;">{{item.id}}<br>{{item.name}}
									</td>
									<td style="width: 25%; text-align: center;"><img
										ng-show="ctrl.generatedControls[$index] == true" height="42"
										width="42" src="images/yes.png"> <img
										ng-show="ctrl.generatedControls[$index] == false" height="42"
										width="42" src="images/no.png">
										<p ng-show="ctrl.generatedControls[$index] == 'Not Available'">Not
											Available</p></td>
									<td style="width: 25%; text-align: center;"><img
										ng-show="ctrl.assessedControls[$index] == true" height="42"
										width="42" src="images/yes.png"> <img
										ng-show="ctrl.assessedControls[$index] == false" height="42"
										width="42" src="images/no.png">
										<p ng-show="ctrl.assessedControls[$index] == 'Not Available'">Not
											Available</p></td>
									<td style="width: 25%; text-align: center;"><img
										ng-show="ctrl.composedControls[$index] == true" height="42" width="42"
										src="images/yes.png"> <img
										ng-show="ctrl.composedControls[$index] == false" height="42"
										width="42" src="images/no.png">
										<p ng-show="ctrl.composedControls[$index] == 'Not Available'">Not
											Available</p></td>
								</tr>
							</tbody>
						</table>

						<div ng-show="ctrl.allControls.length == 0">
							<p>No SLAs Available</p>
						</div>

					</div>
				</div>

			</div>


		</div>

	</div>
</div>

</div>