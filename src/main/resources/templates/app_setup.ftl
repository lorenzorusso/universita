<div class=container>

	<div class="panel panel-default">
		<div class="panel-heading text-center">
			<span class="lead">
				<h2>Import Application from MACM file</h2>
			</span>
		</div>

		<div class="panel-body">

			<div>
				<p>To start with a new application you can select the file that
					contains the MACM representation and then insert the application id contained therein.</p>

				<p>Choose the File</p>

				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<input type="file" id="input-app-macm" />
						</h4>
					</div>
				</div>
				
				<div class="panel panel-default">
					<input type="text" class="form-control" id="appid"
						ng-model="ctrl.appMacmId" placeholder="Application Id">
				</div>
				
				<button class="btn btn-primary" ng-click="ctrl.submitAppMacm()">IMPORT
					MACM</button>
			</div>

		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading text-center">
			<span class="lead">
				<h2>Import Application from MACM</h2>
			</span>
		</div>

		<div class="panel-body">

			<div>
				<p>To import an application from MACM please insert the
					application ID below.</p>

				<div class="panel panel-default">
					<input type="text" class="form-control" id="appid"
						ng-model="ctrl.appMacmId" placeholder="Application Id">
				</div>
				<button class="btn btn-primary" ng-click="ctrl.importAppFromMACM()">IMPORT
					APPLICATION</button>
			</div>

		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading text-center">
			<span class="lead">
				<h2>Open an Application</h2>
			</span>
		</div>

		<div class="panel-body">

			<div>
				<p>To start with an existent application you can select the
					application from the dropdown menu below</p>

				<div class="form-group">
					<label for="sel1">Choose Application to load:</label> <select
						class="form-control"
						ng-options="application as application.id + ' - app Kanban Id: ' + application.kanbanId + ' - app Name: ' + application.name for application in ctrl.applications track by application.id"
						ng-model="selectedApplication"></select>
				</div>

				<div class="form-group">
					<label for="sel1">Choose a Component to set it as active:</label> <select
						class="form-control"
						ng-options="component as component.id + ' - component Kanban Id: ' + component.kanbanId + ' - component Name: ' + component.name for component in selectedApplication.components track by component.id"
						ng-model="selectedComponent"></select>
				</div>

				<button class="btn btn-primary"
					ng-click="ctrl.loadApplicationAndComponent()">OPEN APP</button>
			</div>

		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading text-center">
			<span class="lead">
				<h2>Create a new Application</h2>
			</span>
		</div>

		<div class="panel-body">

			<div>
				<p>To create a new application please fill out the filds below</p>

				<div class="form-group">
					<label for="sel1">Application name:</label> <input type="text"
						class="form-control" id="appname" ng-model="ctrl.appName"
						placeholder="Application Name">
				</div>

				<button class="btn btn-primary" ng-click="ctrl.createNewApp()">CREATE
					APP</button>
			</div>

		</div>
	</div>

	<div style="text-align: center;">

		<button class="btn btn-primary" ng-click="ctrl.removeAllApps()">REMOVE
			ALL APPS</button>

	</div>

</div>