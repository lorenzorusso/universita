
<!DOCTYPE html>

<html lang="en" ng-app="slaGenerator">
<head>
<title>${title}</title>

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">


<link rel='stylesheet'
	href='https://cdnjs.cloudflare.com/ajax/libs/angular-loading-bar/0.8.0/loading-bar.min.css'
	type='text/css' media='all' />


<link rel="stylesheet" href="bootstrap/css/nav-wizard.bootstrap.css">
<link rel="stylesheet" href="bootstrap/css/app.css">

<link rel='stylesheet' href='bootstrap/css/my-loading-bar.css'
	type='text/css' media='all' />
<link rel='stylesheet' href='bootstrap/css/XMLDisplay.css'
	type='text/css' media='all' />

<!-- Graph Visualization -->
<link rel="stylesheet" href="styles/cy2neo.css">
<link rel="stylesheet" href="styles/neod3.css">
<link rel="stylesheet" href="styles/datatable.css">
<link rel="stylesheet" href="styles/vendor.css">


<link rel="stylesheet"
	href="http://framework.musa-project.eu/node_modules/bootstrap/dist/css/bootstrap.min.css">

<link rel='stylesheet'
	href='https://cdnjs.cloudflare.com/ajax/libs/angular-loading-bar/0.8.0/loading-bar.min.css'
	type='text/css' media='all' />

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


</head>
<body>

	<div ui-view="header"></div>
	<div ui-view="content"></div>
	<div ui-view="footer"></div>

	<script src="js/lib/angular.min.js"></script>
	<script src="js/lib/angular-ui-router.min.js"></script>
	<script src="js/lib/localforage.min.js"></script>
	<script src="js/lib/ngStorage.min.js"></script>
	<script src="js/lib/XMLDisplay.js"></script>
	<script src="js/lib/loading-bar.js"></script>

	<!--  Graph visualization libraries -->
	<script src="scripts/vendor.js"></script>
	<script src="scripts/neod3.js"></script>
	<script src="scripts/neod3-visualization.js"></script>
	<script src="scripts/neo4d3.js"></script>
	<script src="scripts/cy2neod3.js"></script>
	<script src="scripts/jquery.dataTables.min.js"></script>
	<script src="scripts/cypher.datatable.js"></script>

	<script src="js/lib/jquery.xml2json.js"></script>



	<script src="js/app/app.js"></script>
	<script src="js/app/SlaGenerationService.js"></script>
	<script src="js/app/SlaGenerationController.js"></script>
	<script src="js/app/SlaViewerService.js"></script>
	<script src="js/app/SlaViewerController.js"></script>
	<script src="js/app/RiskAnalysisService.js"></script>
	<script src="js/app/RiskAnalysisController.js"></script>
	<script src="js/app/HeaderService.js"></script>
	<script src="js/app/HeaderController.js"></script>
	<script src="js/app/AssessmentPageService.js"></script>
	<script src="js/app/AssessmentPageController.js"></script>
	<script src="js/app/AppSetupService.js"></script>
	<script src="js/app/AppSetupController.js"></script>
	<script src="js/app/AppViewService.js"></script>
	<script src="js/app/AppViewController.js"></script>
	<script src="js/app/AppViewServiceV2.js"></script>
	<script src="js/app/AppViewControllerV2.js"></script>
	<script src="js/app/ComposeController.js"></script>
	<script src="js/app/ComposeService.js"></script>
	<script src="js/app/ImplementationPlanController.js"></script>

</body>
</html>